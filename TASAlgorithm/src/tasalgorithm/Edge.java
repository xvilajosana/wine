
package tasalgorithm;

import java.util.Objects;

/**
 *
 * @author jesica
 */
public class Edge {
    private Node node1;
    private Node node2;
    private float quality;
    
    private int load;   // only for directed links
    
    public Edge() {
        
    }
    
    public Edge(Node n1, Node n2, float qual, int l) {
        node1 = n1;
        node2 = n2;
        quality = qual;
        load = l;
    }
    
    public Node getNode1() {
        return node1;
    }
    
    public Node getNode2() {
        return node2;
    }
    
    public float getQuality() {
        return quality;
    }
    
    public int getLoad() {
        return load;
    }
    
    @Override
    public boolean equals(Object other){
        if(this == other) 
            return true; 
        if(other == null || (this.getClass() != other.getClass())) { 
            return false; 
        } 
        Edge otherEdge = (Edge) other; 
        return (this.node1.getId() == otherEdge.node1.getId() && this.node2.getId() == otherEdge.node2.getId()) || (this.node1.getId() == otherEdge.node2.getId() && this.node2.getId() == otherEdge.node1.getId());

    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.node1);
        hash = 53 * hash + Objects.hashCode(this.node2);
        hash = 53 * hash + Float.floatToIntBits(this.quality);
        return hash;
    }
    
    @Override
    public String toString() {
        String s = node1.getId() + " --> " + node2.getId();
        return s;
    }
}

