/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasalgorithm;

import java.util.Comparator;

/**
 *
 * @author jde_armasa
 */
public class PairIndexParentLink implements Comparable<PairIndexParentLink>{
    private int index;
    private float accumulated;
    
    public PairIndexParentLink(int i, float value) {
        index = i;
        accumulated = value;
    }
    
    public float getAcc() {
        return accumulated;
    }
    
    public int getIndex() {
        return index;
    }
    
    @Override
    public int compareTo(PairIndexParentLink other) {
        if (index == other.index)
            return 0;
        else
            return -1;
    }
    
    //@Override
    public static Comparator<PairIndexParentLink> COMPARE_ACCU = new Comparator<PairIndexParentLink>() {

            public int compare(PairIndexParentLink o1, PairIndexParentLink o2) {
                if(o1.getAcc() < o2.getAcc()) {
                    return -1;
                } else if(o1.getAcc() > o2.getAcc()) {
                    return 1;
                }
                return 0;
            }
            
        };

    
}
