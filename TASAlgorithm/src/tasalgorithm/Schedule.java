package tasalgorithm;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author jesica
 */
public class Schedule {
    
    private final int numChannels;
    private final int sizeSlotFrame; 
    private ArrayList<ArrayList<HashSet<Edge>>> schedule;
    private int totalPackLost;
    private int totalNumberPack;
    private int usedSlots;
    
    private ArrayList<HashSet<Edge>> channels;
    
    public Schedule(int ch, int fr) {
        numChannels = ch;
        sizeSlotFrame = fr;
        schedule = new ArrayList<>();
        addFrame();
    }
    
    public void addFrame() {
        for (int i = 0; i < sizeSlotFrame; i++) {
            channels = new ArrayList<>();
            for (int j = 0; j < numChannels;j++) {
                HashSet hs = new HashSet<Edge>();
                channels.add(hs);
            }
            //channels = new ArrayList<>(Collections.nCopies(numChannels, new HashSet<Edge>()));
            schedule.add(channels);
        }
    }
    
    public void setEdge(int slot, int ch, Edge e) {
        schedule.get(slot).get(ch).add(e);
    }
    
    public HashSet<Edge> getEdges(int slot, int ch) {
        return schedule.get(slot).get(ch);             
    }
    
    public int getNumChannels() {
        return numChannels;
    }
    
    public void setLostPackages(int lp) {
        totalPackLost = lp;
    }
    
    public int getLostPackages() {
        return totalPackLost;
    }

    void sendToFile(String outFile) {
        try {
            PrintWriter out = new PrintWriter(outFile);
            out.println("***************************************************");
            out.println("*                      OUTPUTS                    *");
            out.println("***************************************************");
            out.println("\r\n");
            out.println("--------------------------------------------");
            out.println("SCHEDULE");
            out.println("--------------------------------------------");
            out.println(this.toString() + "\r\n");
            out.println("\n\nLost Packages: " + totalPackLost);
            
            out.println("Total Number of Packages: " + totalNumberPack);
            out.println("Slots: " + schedule.size());
            out.println("UsedSlots: " + usedSlots);
            out.close();
        } catch (IOException exception) {
            System.out.println("Error processing output file: " + exception);
        }
    }

    @Override
    public String toString() {
        String s = "\r\n";
        
        for (int i = 0; i < schedule.size(); i++) {
            ArrayList<HashSet<Edge>> listSetEdges = schedule.get(i);
            s += "SLOT " + i + "\r\n" + "+++++++++++++++++++++++" + "\r\n";
            for (int j = 0; j < listSetEdges.size(); j++) { 
                s += "\tChannel " + j + "\r\n";
                s += "\t------------\r\n";
                HashSet<Edge> setEdges = listSetEdges.get(j);
                if (setEdges.isEmpty()) {
                    s += "\t -- " + "\r\n";
                }
                else {
                    //System.out.println("SIZE SET " + setEdges.size());
                    for (Edge e: setEdges) {
                        s += "\t" + e.toString() + "\r\n";
                    }
                }
                s += "\t------------\r\n";
            }
        }       
        return s;
    }

    void setTotalPackages(int num) {
        totalNumberPack = num;
    }

    void setUsedSlots(int slot) {
        usedSlots = slot;
    }

}
