package tasalgorithm;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Random;

/**
 *
 * @author jesica
 */
public class Node {
    private int id;
    private boolean master;
    private int genPackages;                    // q~
    private int waitingPackages;                // q(k)
    private int lostPackages;                   // number of packages that go to this node but do not arrive
    private int numReplicas;
    private int nParentLinks;
    private BitSet receptionBuffer; // only for the master
    private int receptionBufferSize;
    private ArrayList<Integer> slotsReceptionBuffer;
    private ArrayList<ArrayList<Integer>> dispatchBuffer;
    private ArrayList<Integer> commonBuffer;
    private ArrayList<Integer> indexDispatchBuffer;
    
    private float accumulated;
    private boolean adjusted;
    private int indexParentCommonBuffer;
        
    
    public Node(int ident, boolean m, int genP, int numRep, int nlinks) {
        id = ident;
        master = m;
        lostPackages = 0;
        genPackages = genP;
        waitingPackages = 0;
        numReplicas = numRep;
        nParentLinks = nlinks;
        accumulated = 0;
        adjusted = false;
        dispatchBuffer = new ArrayList<>();
        commonBuffer = new ArrayList<>();
        indexDispatchBuffer = new ArrayList<>();
        slotsReceptionBuffer = new ArrayList<>();
        indexParentCommonBuffer = -1;
    }
    
    public int getId() {
        return id;
    }
    
    public int getGenPackages() {
        return genPackages;
    }
    
    public int getLostPackages() {
        return lostPackages;
    }
    
    public int getWaitingPackages(int indexParent) {
        if (master) {
             waitingPackages = 0;
        }
        else {
            if (indexParent != indexParentCommonBuffer)
                //System.out.println("tam buffer propio: " + dispatchBuffer.size() + " tam buffer comun: " +commonBuffer.size());
                waitingPackages = dispatchBuffer.get(indexParent).size(); //+ commonBuffer.size();
            else
                waitingPackages = dispatchBuffer.get(indexParent).size() + commonBuffer.size();
            
            
        }
        return waitingPackages;
    }
    
    public float getAccu() {
        return accumulated;
    }
    
    public boolean getFlag() {
        return adjusted;
    }
    
    public int getIndexCommonBuffer () {
        return indexParentCommonBuffer;
    }
    
    public int getNumParents(){
        return nParentLinks;
    }
    
    public int getNumReplicas() {
        return numReplicas;
    }
    
    public ArrayList<Integer> getDispatchBuffer(int i) {
        return dispatchBuffer.get(i);
    }
    
    public void setIndexCommonBuffer(int i) {
        indexParentCommonBuffer = i;
    }
    
    public void setFlag(boolean f) {
        adjusted = f;
    }
    
    public void setSlotsReceptionBuffer(int index, int slot) {
        slotsReceptionBuffer.set(index, slot);
    }
           
    
    public void printSlotsReception() {
        double total = 0;
        int max = 0;
        for (int i = 0; i < slotsReceptionBuffer.size(); i++) {
            //System.out.print(" " + i + "-" + slotsReceptionBuffer.get(i));
            if (max < slotsReceptionBuffer.get(i))
                max = slotsReceptionBuffer.get(i);
            total += slotsReceptionBuffer.get(i) / (float) slotsReceptionBuffer.size();
        }
        System.out.println(total/max);
    }
    
    public void addLost() {
        lostPackages++;     // Son perdidos totales, no quiere decir que no se retransmitan luego
    }
    
    public boolean isMaster() {
        return master;
    }
    
    public void setAccu(float a) {
        accumulated = a;
    }

    
    public void setDispatchingBuffer (ArrayList<Integer> buffer, int index) {
        ArrayList<Integer> newBuffer = new ArrayList<>(buffer);
        dispatchBuffer.set(index, newBuffer);
    }
    
    public void initDispatchBuffer(int init) {  // init = primer numero , fuera ir sumando numPaq
        ArrayList<Integer> myBuffer = new ArrayList<>();
        for (int j = 0; j < genPackages; j++) {
            myBuffer.add(init + j);
        }
        Collections.shuffle(myBuffer);      // random sort
        dispatchBuffer.add(myBuffer);
        for (int i = 1; i < nParentLinks; i++) {     
            if (i >= numReplicas) {
                ArrayList<Integer> otherBuffer = new ArrayList<>();
                dispatchBuffer.add(otherBuffer);
            }
            else {
                ArrayList<Integer> otherBuffer = new ArrayList<>(myBuffer);
                dispatchBuffer.add(otherBuffer);
            }
            
        }
        /*for (int i = 1; i < numReplicas; i++) {
            ArrayList<Integer> otherBuffer = new ArrayList<>(myBuffer);
            dispatchBuffer.add(otherBuffer);
        }    */
    }
    
    public void insertPackage(int p) {
        commonBuffer.add(p);
    }
    
    public int getNextPackage(Random rand, int indexParent) {
        int p = 0;
        int buffer;
        
        if (indexParent != indexParentCommonBuffer) {
            buffer = 0;
        }
        else {
            if (commonBuffer.isEmpty()) {
                buffer = 0;
            } else {
                if (dispatchBuffer.get(indexParent).isEmpty()) {
                    buffer = 1;
                } else {
                    buffer = rand.nextInt(2);
                }
            }
        }

        if (buffer == 0) {
            // buffer propio
            p = dispatchBuffer.get(indexParent).remove(0);
        }
        else {
            // buffer compartido
            int pos = rand.nextInt(commonBuffer.size());
            p = commonBuffer.remove(pos);
        }
        return p;
    }
    
    public boolean checkReplica(int p) {
        return commonBuffer.remove((Integer) p);
    }
    
    public void retransmit(Random rand, int p) {
        int pos = rand.nextInt(dispatchBuffer.get(indexDispatchBuffer.get(0)).size()+1); 

        if (pos >= dispatchBuffer.get(indexDispatchBuffer.get(1)).size()) {
            dispatchBuffer.get(indexDispatchBuffer.get(0)).add(p);
            dispatchBuffer.get(indexDispatchBuffer.get(1)).add(p);
        }
        else {
            dispatchBuffer.get(indexDispatchBuffer.get(0)).add(pos,p);
            dispatchBuffer.get(indexDispatchBuffer.get(1)).add(pos,p);
        }
        
    /*    int pos = rand.nextInt(dispatchBuffer.get(0).size()+1); // Otra opcion sería no coger el tamaño del primer buffer
        
        for (int i = 0; i < dispatchBuffer.size(); i++) {
            if (pos >= dispatchBuffer.get(i).size())
                dispatchBuffer.get(i).add(p);
            else {
                dispatchBuffer.get(i).add(pos, p);
            }
        }*/
    }
    
    public void initReceptionBuffer(int size) { 
        receptionBuffer = new BitSet(size);//ArrayList<>(Collections.nCopies(size, 0));
        receptionBuffer.clear();
        receptionBufferSize = size;
        for (int i = 0; i < size; i++) {
            slotsReceptionBuffer.add(-1);
        }
    }
    
    public void setReceptionBuffer(int i) {
        receptionBuffer.set(i);
    }
    
    public boolean getReceptionBuffer(int i) {
        return receptionBuffer.get(i);
    }
    
    public int getReceptionBufferCard() {
        ///////System.out.println("RECEPTION BUFFER " + receptionBuffer);
        return receptionBuffer.cardinality();
    }
    public int getReceptionBufferSize() {
        return receptionBufferSize;
    }

    void insertIndexDispatchingBuffer(int index) {
       indexDispatchBuffer.add(index);
    }
    
}
