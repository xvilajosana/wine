package tasalgorithm;


public class Inputs 
{
    /* INSTANCE FIELDS & CONSTRUCTOR */
    private final int nChannels;
    private final int nFrames;
    private final int nNodes;     // numero total de nodos, habra que leerlo de input   
    private final Tree G;
    private final ConnectivityGraph P;
    
    public Inputs(int nCh, int nFr, int nNo, Tree tree, ConnectivityGraph cg) {   
        nChannels = nCh;
        nFrames = nFr;
        nNodes = nNo;
        G = tree;
        P = cg;
    }
    
     /* GET METHODS */
    public int getNumNodes(){return nNodes;}
    public int getNumFrames(){return nFrames;}
    public int getNumChannels(){return nChannels;}
    public Tree getG() {return G;}
    public ConnectivityGraph getConnGraph() {return P;}
}
