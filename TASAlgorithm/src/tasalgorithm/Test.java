package tasalgorithm;
import java.util.Random;


public class Test {   
    final private transient String instanceName; // instance name.
    final private transient String instancePath; // path to the instance file.
    final private transient int seed; // seed for the rng.
    final private transient Random rand; // Random object for shuffles
    
    public Test(final String iName, final String iPath, final int s) 
    {   instanceName = iName;
        instancePath = iPath;
        seed = s;
        rand = new Random(seed); 
    }

    public String getInstanceName(){return instanceName;}
    public String getInstanceFullPath(){return instancePath + instanceName;}

    public int getSeed(){return seed;}
    public Random getRand(){return rand;}

}
