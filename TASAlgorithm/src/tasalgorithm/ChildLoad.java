package tasalgorithm;

/**
 *
 * @author jesica
 * 
 * clase auxiliar para poder ordenar los hijos de mayor a menor carga pendiente
 */
public class ChildLoad implements Comparable<ChildLoad>{
    private int load;
    private int index;
    
    public ChildLoad(int l, int i) {
        load = l;
        index = i;
    }
    
    public int getIndex() {
        return index;
    }
    
    public int getLoad() {
        return load;
    }
    
    // Allows sorting by decreasing load
    @Override
    public int compareTo(ChildLoad other) {
        return other.load - load;
    }

}
