package tasalgorithm;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;

/**
 *
 * @author jesica
 */
public class Tree {
    private ArrayList<Integer> parents; // Los ids de los padres (el orden es el de las colas) CUIDADO!!
    private Node root;
    private ArrayList<Tree> children;
    
    private BitSet nodesIncluded; 
    private ArrayList<Edge> dcfl;   // duplex conflict links

    public Tree(Node r, ArrayList<Tree> ch, ArrayList<Integer> pr) {
        root = r;
        children = new ArrayList<>(ch);
        parents = new ArrayList<>(pr);
    }

    
    public int getQ(int indexParent) { 
        int count = root.getWaitingPackages(indexParent);
        //System.out.println("my pack " + count + " rootid " + root.getId());
        for (Tree t: children) {
            //System.out.println("numero de padres " + t.parents.size());
            int index = t.parents.indexOf(root.getId());
            //System.out.println("index: " + index);
            if (index != -1) {
                 count += t.getQ(index);
            }
        }
        return count;
    }
    
    public int getParent(int index) {
        return parents.get(index);
    }
    
    public ArrayList<Tree> getChildren() {
        return children;
    }
    
    public Node getRoot() {
        return root;
    }
    
    public void addParent(int index) {
        parents.add(index);
    }
    
    public void addChild(Tree n) {
        children.add(n);
    }

    public void removeChild(Tree n) {
        children.remove(n);
    }
    
    public void calculateAccumulated(ConnectivityGraph cg) {
        for (Tree child : children) {
            Node n1 = root;
            Node n2 = child.getRoot();
            Edge e = new Edge(n1, n2, 0, 0);
            float q = cg.findQualityEdge(e, n1.getId());
            float accu = n1.getAccu() + q;
            if (accu > n2.getAccu()) {
                n2.setAccu(accu);
                
            }
        }

      
        for (Tree child : children) {
            child.calculateAccumulated(cg);
        }
         
    }

    /* Move dispatchBuffers to the most promising ways*/
    public void fixDispatchBuffers(ConnectivityGraph cg) {
        for (Tree child : children) {
            ArrayList<Integer> myBuffer = null;
            Node n1 = root;
            Node n2 = child.getRoot();
            // If the node has been adjusted, dont do it again
            if (!n2.getFlag()) {
                int np = n2.getNumParents();
                int nr = n2.getNumReplicas();
                
                Edge e = new Edge(n1, n2, 0, 0);
                float q = cg.findQualityEdge(e, n1.getId());
              
                ArrayList<PairIndexParentLink> pool = new ArrayList<>();
                
                for (int i = 0; i < np; i++) {
                    PairIndexParentLink elem = new PairIndexParentLink(i, q + n1.getAccu());
                    pool.add(elem);
                    //if (pool.size() > nr) {
                    //    pool.remove(pool.last());
                    //}
                    ArrayList<Integer> buffer = n2.getDispatchBuffer(i);
                    
                    if (myBuffer == null && !buffer.isEmpty()) {
                        myBuffer = new ArrayList<>(buffer);
                    }
                    buffer.clear();
                
                }
                
                Collections.sort(pool, PairIndexParentLink.COMPARE_ACCU);

                n2.setIndexCommonBuffer(pool.get(0).getIndex());   
                
                for (int i = 0; i < nr; i++) {
                    n2.setDispatchingBuffer(myBuffer, pool.get(i).getIndex());
                    n2.insertIndexDispatchingBuffer(pool.get(i).getIndex());
                }

                n2.setFlag(true);
            }
            child.fixDispatchBuffers(cg);
        }
    }
    
    public void travelTree(BitSet nIncl, ArrayList<Edge> duplConfLinks) {    
        if (!children.isEmpty()) {
            ArrayList<ChildLoad> childLoad = new ArrayList<>();
            for (int i = 0; i < children.size(); i++) {
                int index = children.get(i).parents.indexOf(root.getId());
                int load = children.get(i).getQ(index);
                //System.out.println("Load: " + load + " del hijo " + children.get(i).getRoot().getId() + " root " + root.getId());
                if (load != 0) //&& children.get(i).getRoot().getWaitingPackages(index) != 0)// Modificar: si no tiene carga propia pero tiene nodos hijos con carga hay que recorrer el arbol pero no seleccionarlo a el!!!
                    childLoad.add(new ChildLoad(load, i));
                
            }
            // sort by load
            Collections.sort(childLoad);

            // include in dcfl the corresponding edge
            boolean done = false;
            while (!done && !childLoad.isEmpty()) {
                Node child = children.get(childLoad.get(0).getIndex()).getRoot();
                int j = children.get(childLoad.get(0).getIndex()).parents.indexOf(root.getId());
                // if none of the nodes (root and child) has been marked
                if ((!nIncl.get(root.getId()) && !nIncl.get(child.getId())) && (child.getWaitingPackages(j) != 0)) {
                    // mark both nodes
                    nIncl.set(root.getId());
                    nIncl.set(child.getId());
                    // create the edge
                    Edge e = new Edge(child, root, 0, childLoad.get(0).getLoad());
                    duplConfLinks.add(e);
                    done = true;
                } else {
                    // if the child had already been included then the branch is not travelled
                    if (nIncl.get(child.getId())) {
                        childLoad.remove(0);
                    } else {
                        // if the root had already been included none of its children has to be included
                        done = true;
                    }
                }
            }
            
            // travel the tree in load decreasing order
            for (ChildLoad cl: childLoad) {
                children.get(cl.getIndex()).travelTree(nIncl, duplConfLinks);     
            }
        }
    }
    
    public ArrayList<Edge> selectEdges(int nNodes) {
        dcfl = new ArrayList<>();
        nodesIncluded = new BitSet(nNodes);
        nodesIncluded.clear();
        
        travelTree(nodesIncluded, dcfl);
        
        return dcfl;
    }

    public int getIndexParent(Node node1, Node node2) {
        int index = -1;
        if (root.getId() != node1.getId()) {
            for (Tree t: children) {
                index = t.getIndexParent(node1, node2);
                //System.out.println("index: " + index);
                if (index != -1)
                    return index;
            } 
        }
        else {
            // buscar el indice de su padre nodo2 y fin
            for (int i = 0; i < parents.size(); i++) {
                if (parents.get(i) == node2.getId()) {
                    //System.out.println("encuentra: " + parents.get(i)+ " pos " + i);
                    return i;
                }
            }
        }
        return index;
    }
    
    public int findIndexParent(Node node1, Node node2) {
        int index = getIndexParent(node1, node2);
        return index;
    }

    public void print() {
        System.out.println("Node: " + root.getId());
        System.out.println("\t Parents: ");
        for (Integer parent : parents) {
            System.out.println(parent);
        }
        System.out.println("\t Children:");
        for (Tree child : children) {
            child.print();
        }
    }


    
}
