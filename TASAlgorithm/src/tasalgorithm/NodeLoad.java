

package tasalgorithm;

/**
 *
 * @author jesica
 */
public class NodeLoad implements Comparable<NodeLoad>{
    private Node node;
    private int load;
    
    
    public NodeLoad(Node n, int l) {
        node = n;
        load = l;
    }
    public Node getNode() {
        return node;
    }
    // Allows sorting by decreasing load
    @Override
    public int compareTo(NodeLoad other) {
        return other.load - load;
    }
    
}
