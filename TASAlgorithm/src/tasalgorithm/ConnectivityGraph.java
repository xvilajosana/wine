package tasalgorithm;

import java.util.ArrayList;

/**
 *
 * @author jesica
 */
public class ConnectivityGraph {
    private ArrayList<ArrayList<Edge>> edgesNode;
    
    public ConnectivityGraph(int nNodes) {
        edgesNode = new ArrayList<>(nNodes);
        
        for (int i = 0; i < nNodes; i++) {
            ArrayList el = new ArrayList<>();
            edgesNode.add(el);
        }
    }
    
    public void insertEdge(Edge e, int node1, int node2){
        edgesNode.get(node1).add(e);
        edgesNode.get(node2).add(e);
        
    }
    
    /* Check if an edge 'e' belongs to the graph */
    public boolean findEdge(Edge e, int node) {  
        return edgesNode.get(node).contains(e);
    }
    
    /* Returns the quality associated to an edge 'e' that contains a node 'node' */
    public float findQualityEdge(Edge e, int node) {
        int index = edgesNode.get(node).indexOf(e);
        if (index != -1)
            return edgesNode.get(node).get(index).getQuality();
        else
            return -1; 
    }
    
    public boolean checkInterference(Edge e1, Edge e2) {
        Node n1 = e1.getNode1();
        Node n2 = e2.getNode1();
        Node p1 = e1.getNode2();
        Node p2 = e2.getNode2();
        
        Edge edge1 = new Edge(n1, n2, 0, 0);
        if (findEdge(edge1, n1.getId()))
            return true;
        Edge edge2 = new Edge(p1, p2, 0, 0);
        if (findEdge(edge2, p1.getId()))
            return true;
        Edge edge3 = new Edge(n1, p2, 0, 0);
        if (findEdge(edge3, n1.getId()))
            return true;
        Edge edge4 = new Edge(n2, p1, 0, 0);
        if (findEdge(edge4, n2.getId()))
            return true;

        return false;
    }
    
}
