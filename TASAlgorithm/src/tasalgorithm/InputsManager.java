package tasalgorithm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Scanner;


public class InputsManager {

    public static Inputs readInputs(String inputsPath) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(inputsPath));
        Scanner in = new Scanner(reader);
        Inputs inputs;

        in.useLocale(Locale.US);

        in.nextLine();

        int nCh = in.nextInt();
        int nFr = in.nextInt();
        int nNo = in.nextInt();

        in.nextLine();
        in.nextLine();
        
        ArrayList<Integer> nGen = new ArrayList<>();
        nGen.add(0);
        for (int i = 1; i < nNo; i++) {
            in.nextInt();
            nGen.add(in.nextInt());
            //System.out.println("nGen: " + nGen.get(i));
            in.nextLine();
        }

        in.nextLine();
        
        int nRep = in.nextInt();
        //System.out.println("replicas: " + nRep);
        int nParentLinks = in.nextInt();
        ConnectivityGraph cg = new ConnectivityGraph(nNo);
        in.nextLine();
        in.nextLine();
        in.nextLine();
        
        Node nodeList[] = new Node[nNo];    // duda si se inicializa a null
        int initialNumber = 0;
        String s = null;
        s = in.next();
        // Create connectivity graph
        while (s.charAt(0) != '#') {
            try {
                int n1 = Integer.parseInt(s);
                int n2 = in.nextInt();
                float q = in.nextFloat();
                int m = in.nextInt();
                //System.out.println("n1: " + n1 + " n2: " + n2 + " m " + m);
                boolean master = false;
                if (m == 1) {
                    master = true;
                }
                if (nodeList[n1] == null) {
                    Node node1 = new Node(n1, master, nGen.get(n1), nRep, nParentLinks);
                    node1.initDispatchBuffer(initialNumber);
                    if (n1 != 0)
                        initialNumber += nGen.get(n1);          // Increase the number of the next package for the next node
                    nodeList[n1] = node1;
                }
                if (nodeList[n2] == null) {
                    Node node2 = new Node(n2, false, nGen.get(n2), nRep, nParentLinks);
                    node2.initDispatchBuffer(initialNumber);
                    if (n2 != 0)
                        initialNumber += nGen.get(n2); 
                    nodeList[n2] = node2;
                }

                Edge e = new Edge(nodeList[n1], nodeList[n2], q, 0);
                cg.insertEdge(e, n1, n2);

                s = in.next();

            } catch (NoSuchElementException nsee) {

            }
        }

        nodeList[0].initReceptionBuffer(initialNumber);
        
        in.nextLine();
        in.nextLine();

        // Create the tree
        ArrayList<Tree> queue = new ArrayList<>();
        boolean endOfFile = false;
        
        
        
        // Nuevo
        for (Node n: nodeList) {
            ArrayList<Integer> pr = new ArrayList<>();
            ArrayList<Tree> ch = new ArrayList<>();
            Tree t = new Tree(n, ch, pr);
            queue.add(t);
        }
 
        while (in.hasNextLine() && endOfFile == false) {
            try {
                String codes = in.nextLine();
                String code[] = codes.split("\t");
                int nodeId = Integer.parseInt(code[0]);
                Tree st = null;
                // Check if the node exists in the queue
                for (int j = 0; j < queue.size(); j++) {
                    if (queue.get(j).getRoot().getId() == nodeId) {
                        st = queue.get(j);
                        break;
                    }
                }

                for (int i = 1; i < code.length; i++) {
                    int parentId = Integer.parseInt(code[i]);
                    st.addParent(parentId);
                    Tree st2 = null;
                    // Check if it exists in the queue
                    for (int j = 0; j < queue.size(); j++) {
                        if (queue.get(j).getRoot().getId() == parentId) {
                            st2 = queue.get(j);
                            break;
                        }
                    }
                    st2.addChild(st);
                    
                }

            } catch (NoSuchElementException nsee) {
                endOfFile = true;
            }
 
        }

        Tree tree = null;
        for (Tree q : queue) {
            if (q.getRoot().getId() == 0) {
                tree = q;
                break;
            }
        }

       /* 
        // Create leaves 
        String codes = in.nextLine();
        String code[] = codes.split("\t");
        
        for (String code1 : code) {
            int nodeId = Integer.parseInt(code1);
            //System.out.println("nodeID: " + nodeId);
            Node n = nodeList[nodeId];
            ArrayList<Integer> pr = new ArrayList<>();
            ArrayList<Tree> ch = new ArrayList<>();
            Tree t = new Tree(n, ch, pr);
            queue.add(t);
        }
        in.nextLine();
        
        // Create other nodes of the tree
        while (in.hasNextLine() && endOfFile == false) {
            try {
                // Extract from the queue
                Tree t = queue.remove(0);
                codes = in.nextLine();
                code = codes.split("\t");
                
                // Insert all their parents and create new trees for the parents
                for (int i = 1; i < code.length; i++) {
                    int parentId = Integer.parseInt(code[i]);
                    //System.out.println("node: " + parentId);
                    // Add the parent to its list of parents
                    t.addParent(parentId);
                    // Check if it exists in the queue
                    int iterator = -1;
                    for (int j = 0; j < queue.size(); j++) {
                        if (queue.get(j).getRoot() == nodeList[parentId]) {
                            iterator = j;
                            break;
                        }
                    }
                    // If it does not exist, create it ant insert it
                    if (iterator == -1) {
                        ArrayList<Integer> pr = new ArrayList<>();
                        ArrayList<Tree> ch = new ArrayList<>();
                        Node n = nodeList[parentId];
                        ch.add(t);
                        Tree st = new Tree(n, ch, pr);
                        queue.add(st);
                    }
                    // If it exists, add the new child
                    else {
                        queue.get(iterator).addChild(t);
                    }
                }  
            } catch (NoSuchElementException nsee) {
                endOfFile = true;
            }  
        }
        
        Tree tree = queue.remove(0);*/
        
        //tree.print();

        inputs = new Inputs(nCh, nFr, nNo, tree, cg);
        return inputs;
    }
}
