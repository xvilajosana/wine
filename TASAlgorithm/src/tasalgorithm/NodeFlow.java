/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tasalgorithm;

/**
 *
 * @author jesica
 */
public class NodeFlow {
    private int idnode;
    private int flow;
    
    public NodeFlow(int n, int f) {
        idnode = n;
        flow = f;
    }
    
    public int getNode() {
        return idnode;
    }
    
    public int getFlow() {
        return flow;
    }
    
    public void increaseFlow() {
        flow++;
    }
}
