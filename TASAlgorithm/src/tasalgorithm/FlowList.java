package tasalgorithm;

import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jesica
 */
public class FlowList {
    ArrayList<ArrayList<NodeFlow>> list;
    
    public FlowList(int size, Tree t) {
        list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add(new ArrayList<NodeFlow>());
        }
        addLinks(t);
    }
    
    public final void addLinks(Tree t) {
        int n2 = t.getRoot().getId();

        for (Tree ch : t.getChildren()) {
            NodeFlow nf = new NodeFlow(n2, 0);
            if (!has(ch.getRoot().getId(), n2))
                list.get(ch.getRoot().getId()).add(nf);
            
            addLinks(ch);
        }
       
    }
    public boolean has(int index, int node) {
        for (NodeFlow nf: list.get(index)) {
            if (nf.getNode() == node) {
                return true;
            }
        }
        return false;
    }
    
    public float calcAvgFlow() {
        float avg;
        int count = 0;
        int sum = 0;
        
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.get(i).size(); j++) {
                //System.out.println("node" + i + " --> node" + list.get(i).get(j).getNode() + ": " + list.get(i).get(j).getFlow());
                sum += list.get(i).get(j).getFlow();
                count++;
            }
        }
        
        avg = (float) sum / (float) count;

        return avg;
    }

    void addFlow(Edge e) {
        for (NodeFlow nf: list.get(e.getNode1().getId())) {
            if (nf.getNode() == e.getNode2().getId()) {
                nf.increaseFlow();
                break;
            }
        }
    }
    
}
