

package tasalgorithm;

import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author jesica
 */
public class InterferenceConflictGraph {
    private ArrayList<Edge> eNodes;
    private HashSet<Edge> interferingEdges;
    
    public InterferenceConflictGraph(ArrayList<Edge> d) {
        eNodes = new ArrayList<>(d);
        interferingEdges = new HashSet<>();
    }
    

    /* Create the set of interfering edges */
    public void calculateInterferenceEdges(ConnectivityGraph cg) {
        for (int i = 0; i < eNodes.size() - 1; i++) {
            for (int j = i+1; j < eNodes.size(); j++) {
                Edge e1 = eNodes.get(i);
                Edge e2 = eNodes.get(j);
                
                if (cg.checkInterference(e1, e2)) {
                    Edge e = new Edge(e1.getNode1(), e2.getNode1(), 0, 0);
                    interferingEdges.add(e);
                } 
            }
        }
    }
    public boolean checkInterference(Edge e) {
        Edge e2 = new Edge(e.getNode2(), e.getNode1(),0 , 0);
        return interferingEdges.contains(e) || interferingEdges.contains(e2);
    }
    public void printInterferingEdges () {
        for (Edge e: interferingEdges) {
            System.out.println(e.getNode1().getId() + " -> " + e.getNode2().getId());
        }
    }
    
}
