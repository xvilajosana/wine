package tasalgorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;

/**
 *
 * @author jesica
 */
public class TASAlgorithm {

    private int nNodes;
    private int nFrames;
    private Tree tree;
    private ConnectivityGraph connGraph;
    private Random rand;

    private int totalPackLost;
    private Schedule schedule;
    private ArrayList<Edge> dcfl;    // duplex conflict links
    private InterferenceConflictGraph icfg;  // interference conflict graph
    private ArrayList<HashSet<Node>> setList;
    private ArrayList<Integer> retransmitCount;

    private FlowList flowList;

    public TASAlgorithm(Test test, Inputs input) {
        nFrames = input.getNumFrames();
        nNodes = input.getNumNodes();
        tree = input.getG();
        connGraph = input.getConnGraph();
        rand = test.getRand();
        totalPackLost = 0;
        // Initialize schedule
        schedule = new Schedule(input.getNumChannels(), input.getNumFrames());
        flowList = new FlowList(nNodes, tree);
        // Initialize counters for retransmition
        retransmitCount = new ArrayList<>();
        for (int i = 0; i < nNodes; i++)
            retransmitCount.add(0);
    }

    public void matching() {
        if (dcfl != null) {
            dcfl.clear();
        }
        dcfl = tree.selectEdges(nNodes);
    //    System.out.println("DUPLEX CONFLICT FREE EDGES:");
        //    for (int i = 0; i < dcfl.size(); i++) {
        //        System.out.println( dcfl.get(i).getNode1().getId() + " --> " +  dcfl.get(i).getNode2().getId());
        //    }
    }

    public void colouring() {
        if (setList != null) {
            setList.clear();
        }
        ArrayList<NodeLoad> nList = new ArrayList<>();
        for (Edge e : dcfl) {
            NodeLoad nl = new NodeLoad(e.getNode1(), e.getLoad());
            nList.add(nl);
        }
        Collections.sort(nList);
        setList = new ArrayList<>();

        while (!nList.isEmpty()) {
            // create a set
            HashSet<Node> nodeSet = new HashSet<>();
            NodeLoad chosenNode = nList.remove(0);
            // introduce the first node in the set
            nodeSet.add(chosenNode.getNode());

            ArrayList<Integer> indexList = new ArrayList<>();
            // create the list of indexes without intereferences with the node
            for (int i = 0; i < nList.size(); i++) {
                Edge e = new Edge(chosenNode.getNode(), nList.get(i).getNode(), 0, 0);
                if (!icfg.checkInterference(e)) {
                    indexList.add(i);
                }
            }

            ArrayList<Integer> indexesToRemove = new ArrayList<>();
            while (!indexList.isEmpty()) {
                Integer index = indexList.remove(0);
                NodeLoad otherNode = nList.get(index);
                indexesToRemove.add(index);

                // Introduce the first node of the list in the set
                nodeSet.add(otherNode.getNode());
                //System.out.println("indexList size: " + indexList.size() + " nList size: " + nList.size());
                // Remove indexes of nodes with interferences 
                for (int i = 0; i < indexList.size(); i++) {
                    Edge e = new Edge(otherNode.getNode(), nList.get(indexList.get(i)).getNode(), 0, 0);
                    if (icfg.checkInterference(e)) {
                        int indexToRem = indexList.remove(i);
                        //System.out.println("IndexToRem: " + indexToRem);
                        --i;
                        indexesToRemove.add(indexToRem);
                    }
                }
            }

            Collections.sort(indexesToRemove);
            for (int i = indexesToRemove.size() - 1; i >= 0; i--) {
                int ind = indexesToRemove.get(i);
                nList.remove(ind);
            }

            setList.add(nodeSet);
        }
        //System.out.println("COMPATIBLE SETS");
        //for (HashSet<Node> s: setList) {
        //    System.out.println("Set: ");
        //    for (Node n: s) {
        //        System.out.println("\t" + n.getId());
        //    }
        //}
    }

    public void createInterfConflictGraph() {
        // Insert all (ni -> pi) belonging to dcfl 
        icfg = new InterferenceConflictGraph(dcfl);
        // Create interference edges
        icfg.calculateInterferenceEdges(connGraph);
        //System.out.println("INTERFERENCE CONFLICT GRAPH: ");
        //icfg.printInterferingEdges();

    }

    public void updateBuffers(Edge e, int slot) {
        float quality = connGraph.findQualityEdge(e, e.getNode1().getId());
        //System.out.println("n1: " + e.getNode1().getId() + " n2: " + e.getNode2().getId());
        float prob = rand.nextFloat();
        //System.out.println("QUALITY: " + prob);
        if (prob > quality) {
            // Lost package
            e.getNode2().addLost();     // the package goes to this node and does not arrive
            totalPackLost++;
            // Get index of parent node2 regarding node1
            int indexParent = tree.findIndexParent(e.getNode1(), e.getNode2());
            // Get package
            // If it has been tried less than 3 time, dont extract and increase the counter
            int counter = retransmitCount.get(e.getNode1().getId());
            if (counter < 3) {
               retransmitCount.set(e.getNode1().getId(), counter + 1);
            }
            else {
                int packg = e.getNode1().getNextPackage(rand, indexParent);
                retransmitCount.set(e.getNode1().getId(), 0);
            }
            
            
            //System.out.println("LOST package: " + packg);
        } else {
            // Get package
            int indexParent = tree.findIndexParent(e.getNode1(), e.getNode2());
            //System.out.println("indexParent " + indexParent);
            int packg = e.getNode1().getNextPackage(rand, indexParent);
            // If the receptor is the master node, set the bit
            if (e.getNode2().getId() == 0) {
                if (!e.getNode2().getReceptionBuffer(packg)) { //.isMaster()) {
                    e.getNode2().setReceptionBuffer(packg);
                    e.getNode2().setSlotsReceptionBuffer(packg, slot);
                }
            } else {
                // Looking for a replica in the common buffer (it deletes the replica)
                if (e.getNode2().checkReplica(packg)) {
                    // Insert replicas in each dispatching buffer 
                    e.getNode2().retransmit(rand, packg);
                } else {
                    e.getNode2().insertPackage(packg);
                }
            }
        }
    }

    public void updateSchedule(int slot) {
        int i = 0;
        // Each set in setList can be plan in the same channel of the slot
        for (HashSet<Node> s : setList) {
            //System.out.println("Slot: " + slot + " channel " + i);
            // If there are more sets than channels, the rest are for the next slots?? Estoy desechando y no estoy segura
            if (i == schedule.getNumChannels()) {
                break;
            }

            // All these edges in the set can be inserted in the same channel
            for (Node n : s) {
                // Find the edge in the list
                for (Edge e : dcfl) {
                    if (e.getNode1().getId() == n.getId()) {
                        // Insert it in the schedule
                        //System.out.println("Slot " + slot + " channel: " + i + " edge " + e.getNode1().getId());
                        schedule.setEdge(slot, i, e);
                        flowList.addFlow(e);
                        updateBuffers(e, slot);
                        break;
                    }
                }
            }
            i++;
        }
    }

    
    public Schedule solve() {
        // Calculate accumulated links qualities
        tree.calculateAccumulated(connGraph);
        // Move dispatchBuffers to the most promising ways
        tree.fixDispatchBuffers(connGraph);
 
        int slot = 0;
        while (tree.getQ(-1) != 0) {
            //System.out.println("iter: " + slot);
            // if we need more slots than the size of a frame
            if (slot != 0 && slot % nFrames == 0) {
                // insert new slotframe in schedule
                schedule.addFrame();
            }
            matching();
            createInterfConflictGraph();
            colouring();
            updateSchedule(slot);
            slot++;

        }
        
        float arrivPercentage = (float) tree.getRoot().getReceptionBufferCard() / tree.getRoot().getReceptionBufferSize();
        System.out.print(arrivPercentage + "\t" + flowList.calcAvgFlow() + "\t" + slot + "\t");
        
        tree.getRoot().printSlotsReception();   // print time slot for each package
        
        //System.out.println("Lost Packages: " + totalPackLost + " Num pack: " + tree.getRoot().getReceptionBufferSize() + " Received pack: "+ tree.getRoot().getReceptionBufferCard());
        //System.out.println("Slots: " + slot);
        schedule.setLostPackages(totalPackLost);
        schedule.setTotalPackages(tree.getRoot().getReceptionBufferSize());
        schedule.setUsedSlots(slot);
        //System.out.println(flowList.calcAvgFlow());
        return schedule;
    }

}
