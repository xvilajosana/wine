package tasalgorithm;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class AlgorithmTester 
{
    private final static Logger LOGGER = Logger.getLogger("");
    private final static String INPUT_FOLDER = "inputs";
    private final static String OUTPUT_FOLDER = "outputs";
    private final static String TEST_FOLDER = "tests";
    private final static String FILE_NAME_EXT = ".txt";
    private final static String[] TEST_TO_RUN_LIST = new String[]{
      //  "prueba_BK_Dq_6"
      //"prueba_BK_B",
       "test2run1",
       "test2run2",
       "test2run3",
       "test2run4",
       "test2run5",
       "test2run6",
       "test2run7",
       "test2run8",
       "test2run9",
       "test2run10",
    };

    public static void main(final String[] args) throws IOException 
    {
        System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
        final long programStart = ElapsedTime.systemTime();
        ////////LOGGER.setLevel(Level.ALL);

        /* 1. GET THE LIST OF TESTS TO RUN */
        final ArrayList<Test> testsList = TestsManager.getTestsList(TEST_FOLDER + 
                File.separator, TEST_TO_RUN_LIST, FILE_NAME_EXT);
        System.out.println("GG");
        /* 2. FOR EACH TEST IN THE LIST... */
        
        FWriterCheckTests.open(System.currentTimeMillis() + "_");

        for( int k = 0; k < testsList.size(); k++ ) 
        {
            Test aTest = testsList.get(k);
            ///////LOGGER.log(Level.INFO, "[{0}] Test for instance {1} prepared ({2}/{3})", new Object[]{ElapsedTime.calcElapsed(programStart,  ElapsedTime.systemTime()), aTest.getInstanceName(), k + 1, testsList.size()});

            // 2.1 GET THE INSTANCE INPUTS
            Inputs someInputs = InputsManager.readInputs(INPUT_FOLDER + 
                    File.separator + aTest.getInstanceFullPath());
            
            
            // 2.2. USE THE METAHEURISTIC ALGORITHM TO SOLVE THE INSTANCE
            TASAlgorithm ta = new TASAlgorithm(aTest, someInputs);
            
            Schedule sch  = ta.solve();
            
            //FWriterCheckTests.write(aTest.getInstanceName() + "\t" + aTest.getSeed() + "\t" + sch.getLostPackages() +"\n");

             // 2.3. PRINT OUT THE RESULTS TO FILE
            String outputsFilePath = OUTPUT_FOLDER + File.separator + aTest.getInstanceName() + "_" + aTest.getSeed() + FILE_NAME_EXT; 
            sch.sendToFile(outputsFilePath); 
        }
        FWriterCheckTests.finish();
        /* 3. END OF PROGRAM */
        LOGGER.log(Level.INFO, "[{0}] All tests ended correctly, saving data", 
                ElapsedTime.calcElapsed(programStart, ElapsedTime.systemTime()));
        LOGGER.log(Level.WARNING, "[{0}] Full program execution ended correctly "
                + "in {1}", new Object[]{ElapsedTime.calcElapsed(programStart, 
                ElapsedTime.systemTime()), ElapsedTime.calcElapsedHMS(programStart, 
                ElapsedTime.systemTime())});        
    }
}
